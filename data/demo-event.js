demo_event = {
  id: 1,
  title: 'Presentació del llibre "Transgresorxs"',
  slug: 'presentacio-del-llibre-transgresorxs',
  description: '<p>Presentació del llibre "Transgresorxs" a càrrec de Bru Madrenas, en acabat hi haurà kafeta</p>',
  multidate: false,
  start_datetime: 1657126800,
  end_datetime: 1657139400,
  image_path: null,
  media: [
    {
      url: '27af5196c795fb4161eebe9c8f7643de.jpg',
      height: 800,
      width: 566,
      name: 'Presentació del llibre "Transgresorxs"',
      focalpoint: [Array]
    }
  ],
  is_visible: true,
  recurrent: null,
  likes: [],
  boost: [],
  createdAt: 2022-07-04T14:54:01.231Z,
  updatedAt: 2022-07-04T15:26:41.356Z,
  placeId: 1,
  userId: 1,
  parentId: null,
  tags: [
    {
      tag: 'kafeta',
      createdAt: 2022-07-04T15:17:25.301Z,
      updatedAt: 2022-07-04T15:17:25.301Z,
      event_tags: [Object]
    },
    {
      tag: 'presentació llibre',
      createdAt: 2022-07-04T14:54:01.268Z,
      updatedAt: 2022-07-04T14:54:01.268Z,
      event_tags: [Object]
    },
    {
      tag: 'transfeminisme',
      createdAt: 2022-07-04T14:54:01.268Z,
      updatedAt: 2022-07-04T14:54:01.268Z,
      event_tags: [Object]
    }
  ],
  place: {
    id: 1,
    name: 'Ateneu llibertari la zitzània',
    address: 'Carrer Muntanya 96',
    createdAt: 2022-07-04T14:54:01.131Z,
    updatedAt: 2022-07-04T14:54:01.131Z
  },
  notifications: [
    {
      id: 1,
      filters: [Object],
      email: null,
      remove_code: null,
      action: 'Create',
      type: 'ap',
      createdAt: 2022-07-04T14:51:19.265Z,
      updatedAt: 2022-07-04T14:51:19.265Z,
      event_notification: [Object]
    },
    {
      id: 2,
      filters: [Object],
      email: null,
      remove_code: null,
      action: 'Update',
      type: 'ap',
      createdAt: 2022-07-04T14:51:19.265Z,
      updatedAt: 2022-07-04T14:51:19.265Z,
      event_notification: [Object]
    }
  ],
  user: {
    id: 1,
    settings: [],
    email: 'admin',
    description: null,
    password: '$2a$10$RzEuxb.thisisprotectedbuthuh.anywayyoudontneeditdoyou",
    recover_code: null,
    is_admin: true,
    is_active: true,
    createdAt: 2022-07-04T14:51:36.834Z,
    updatedAt: 2022-07-04T14:51:36.834Z
  }
}
