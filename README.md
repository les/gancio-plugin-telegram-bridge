# Telegram bridge for Gancio

This is an attempt to create a plugin for Gancio that republishes content to Telegram channels or groups. The goal is to spread the info of our networks to the capitalist cyberspace, and pull otherwise isolated people to our radical and free part of the internet.

## Clone this repo to your Gancio

I don't know how to install this plugin side by side to other plugins. So let's do it like this by now:

```bash
# assuming docker installation as per https://gancio.org/install/docker
cd /opt/gancio/data
git clone https://framagit.org/bcn.convocala/gancio-plugin-telegram-bridge.git
mv gancio-plugin-telegram-bridge plugins
```

## Configuration


Once the plugin is installed, navigate to your instance plugins tab of the admin interface. Enable the plugin, and add the required data: 
* Your Telegram's **bot token** you want to impersonate with this plugin. [Help on telegram bots](https://core.telegram.org/bots#6-botfather)
* The **channel/group id** where to publish the messages. [Help on getting chat ids](https://github.com/GabrielRF/telegram-id)


## Try

1. Restart your gancio instance and look at the logs for any message saying that this plugin has been loaded.
2. Edit an existing event and save it just like that (this counts as an update)
3. Check your telegram channel for new activities


## Rough roadmap

### v0.1.0
- [x] Send plain text messages with event info
- [x] Send photos with event info

### v1.0.0
- [ ] Test plugin with public instance. Event URLs from dev env are unreachable for telegram to retrieve info.
- [ ] Check for Telegram API limits: image size, description text lenght
- [ ] Ensure gancio event description text is cleaned for limited html telegram support (like `<p>` and `<h1>`)
- [ ] Ignore update and delete by now.

### More
- [ ] Register created or modified events to an sqlite db
- [ ] Use telegram edit feature to modify sent events
- [ ] Use telegram delete feature to remove deleted events
- [ ] Register commands to edit config, and to add and remove bot admin telegram accounts
- [ ] Fill properly npm package.json
- [ ] Add tests
- [ ] Allow to send to multiple channels
- [ ] Allow to send from multiple bots
