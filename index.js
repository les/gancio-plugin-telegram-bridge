// TODO: how do we install plugin dependencies?
const { Telegraf } = require('telegraf')

const plugin = {
  configuration: {
    name: 'Telegram',
    author: 'fadelkon',
    url: 'https://framagit.org/bcn.convocala/gancio-plugin-telegram-bridge',
    description: 'Republishes content to Telegram channels or groups. The goal is to spread the info of our networks to the capitalist cyberspace, and pull otherwise isolated people to our radical and free part of the internet.',
    settings: {
      auth_token: {
        type: 'TEXT',
        description: 'Auth token',
        required: true,
        hint: 'Your Telegram\'s <strong>bot token</strong> you want to impersonate.<br/><a href="https://core.telegram.org/bots#6-botfather">Help on telegram bots</a>'
      },
      chat_id: {
        type: 'TEXT',
        description: 'Channel id',
        required: true,
        hint: 'The <strong>channel/group id</strong> where to publish the messages.<br/><a href="https://github.com/GabrielRF/telegram-id">Help on getting chat ids</a>'
      }
    }
  },
  gancio: null,
  settings: null,
  load(gancio, settings) {
    plugin.gancio = gancio
    plugin.settings = settings
  },
  onEventCreate(event) {
    if (event.is_visible) {
      const bot = new Telegraf(plugin.settings.auth_token)
      let image_url = `${plugin.gancio.settings.baseurl}/logo.png`
      if (event.media && event.media.length) {
        image_url = `${plugin.gancio.settings.baseurl}/media/${event.media[0].url}`
      }
      bot.telegram.sendPhoto(plugin.settings.chat_id, image_url, { "parse_mode": "HTML", "caption": renderEvent(event) });
    }
  },

  onEventUpdate(event) {
    console.error(`Event "${event.title}" updated`)
    console.error(event)
  },

  onEventDelete(event) {
    console.error(`Event "${event.title}" deleted`)
    console.error(event)
  }
}

function renderEvent(event) {
  return `\
<b>${event.title}</b>

📅 ${(new Date(event.start_datetime * 1000)).toLocaleString()}
📌 ${event.place.name}

${event.description.replace(/<p>/g, "").replace(/<\/p>/, "\n")}

${event.tags.reduce((x, y) => { return x + " #" + y.tag.replaceAll(" ", "_") }, "").trim()}
<a href="${plugin.gancio.settings.baseurl}">${plugin.gancio.settings.title}</a>
`
}

module.exports = plugin
